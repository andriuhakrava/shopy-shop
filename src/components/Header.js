import React from 'react';
import Logo from './Logo';
import Navigation from './Navigation';
import './Header.css';
import mailIcon from '../assets/images/mail-icon.png';
import phoneIcon from '../assets/images/phone-icon.png';
import socialFacebook from '../assets/images/social-facebook.png';
import socialTwitter from '../assets/images/social-twitter.png';
import socialGoogleplus from '../assets/images/social-googleplus.png';
import socialInstagram from '../assets/images/social-instagram.png';
import searchIcon from '../assets/images/search-icon.png';
import userIcon from '../assets/images/user-icon.png';
import cartIcon from '../assets/images/cart-icon.png';

class Header extends React.Component {	
	render(){
		return (
			<header>
				<div className = "row-wrap-header">
					<div className="container">
						<div className = "row">
							<div className="col-md-3">
								<div className= "contacts-header">
									<ul className="contacts-header-links">
										<li>
											<a href="/">
												<img src={mailIcon} alt="mail-icon" />
												<span className="contacts-header-links-content">info@shopy.com</span>
											</a>
										</li>
										<li>
											<a href="/">
												<img src={phoneIcon} alt="phone-icon" />
												<span className="contacts-header-links-content">996 - 5553 - 453</span>
											</a>
										</li>
									</ul>
								</div>	
							</div>
							<div className = "col-md-2 offset-md-7">
								<ul className="social-links">
									<li>
										<a href="/" title="shopy in facebook">
											<img src={socialFacebook} alt="social-facebook"/>
										</a>
									</li>
									<li>
										<a href="/" title="shopy in twitter">
											<img src={socialTwitter} alt="social-twitter"/>
										</a>
									</li>
									<li>
										<a href="/" title="shopy in googleplus">
											<img src={socialGoogleplus} alt="social-googleplus"/>
										</a>
									</li>
									<li>
										<a href="/" title="shopy in instagram">
											<img src={socialInstagram} alt="social-instagram"/>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div className="row-wrap-header row-wrap-header_main">
					<div className="container">
						<div className = "row">	
							<div className="col-md-2">
								<Logo />
							</div>
							<div className="col-md-5">
								<nav className="header-navigation">
									<Navigation />
								</nav>
							</div>
							<div className="col-md-2 offset-md-3">
								<ul className="user-controls">
									<li><a href="/"><img src={searchIcon} alt="search-icon"/></a></li>
									<li><a href="/"><img src={userIcon} alt="user-icon"/></a></li>
									<li><a href="/"><img src={cartIcon} alt="cart-icon"/></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</header>
		)
	}
}

export default Header;